# Desafio6

1. **Criar um cluster na Aws para execução das pipelines:**


``````
kops create cluster --state "s3://cluster-state-ths" --zones "us-east-1a,us-east-1b" --master-count 1 --master-size=t3a.medium --vpc=${VPC_ID} --node-count 2 --node-size=t3a.medium --name desafio01thiago.tk --yes

export VPC_ID=

```````
**Link Documentação**

``````
https://www.poeticoding.com/create-a-high-availability-kubernetes-cluster-on-aws-with-kops/
``````


2. **Realizar a instalação do Kubernetes e Kube config, Documentação a ser seguida :**


Realizar a instalação seguindo a Documentação abaixo
``````
https://kubernetes.io/pt/docs/setup/
``````

Criar kubeconfig manualmente

Crie o diretório ~/.kube padrão.

``````
mkdir -p ~/.kube

``````
Abra o editor de texto de sua preferência e copie um dos blocos de código kubeconfig abaixo, dependendo do método de token do cliente da sua preferência

``````
apiVersion: v1
clusters:
- cluster:
    server: <endpoint-url>
    certificate-authority-data: <base64-encoded-ca-cert>
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    user: aws
  name: aws
current-context: aws
kind: Config
preferences: {}
users:
- name: aws
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1alpha1
      command: aws
      args:
        - "eks"
        - "get-token"
        - "--cluster-name"
        - "<cluster-name>"
        # - "--role"
        # - "<role-arn>"
      # env:
        # - name: AWS_PROFILE
        #   value: "<aws-profile>"
        
``````
Verificar o Arquivo:

``````
echo $(cat ~/.kube/config | base64) 
``````



3. **Criando namespace**

- kubectl create namespace <namespace name>

**Verificar se foi criado**

- kubectl get namespaces

Retorno;
``````
NAME                  STATUS   AGE
crisdesafio6          Active   14d
``````



4. **Criar o arquivo secret no seu diretório /Desafio6 :**

``````
kubectl create secret generic desafiocris6 --from-literal=banco=xxxxx --from-literal=user=xxxxx --from-literal=password=passwd --from-literal=db=xxxxx --from-literal=access=xxxxx --from-literal=secret=xxxxx --from-literal=region=us-east-1 --from-literal=fila=xxxxx --from-literal=nosql=xxxxx --from-literal=REDIS_PORT=xxxxx --from-literal=AWS_ACCESS_KEY=xxxxx --from-literal=AWS_SECRET_KEY=xxxxx --from-literal=AWS_DEFAULT_REGION=us-east-1 --from-literal=nome_fila=xxxxx -n crisdesafio6
```````

5. ** Conectar com seu Gitlab CI :**

**Comandos:**

- git lab init
- git remote add origin https://gitlab.com/empresa-teste/projeto-x.git
- git status
- git commit -m "versao"
- git push origin master


6. ** Conectar com seu Gitlab CI:**

*O Sistema usado para realizar o Deploy será o Blue-Green*

- O deploy blue-green consiste em ter dois ambientes idênticos (também chamado de mirror), cujo na frente desses ambientes exista um load balancer que permita o direcionamento do tráfego para o ambiente desejado.



7. **Criar o arquivo .gitlab-ci.yml /Desafio6 :**


``````
image: ubuntu:latest
services:
  - docker:dind

stages:
  - teste
  - build
  - docker-build-image
  - deploy_k8s
  - rollout

variables:
  NAME: cristianofeliciano/crisdesafio6
  TAG: latest
  IMG: ${NAME}:${CI_PIPELINE_ID}
  LATEST: ${NAME}:latest

docker-build-image:
  stage: docker-build-image  
  image: docker:dind
  script:
    - echo "LOGANDO NO DOCKER HUB"
    - docker login -u ${DOCKER_USER} -p ${DOCKER_PASSWORD}
    - docker build -t ${CONTAINER_IMAGE} .
    - docker tag ${CONTAINER_IMAGE} ${CONTAINER_IMAGE}
    - echo "BAIXANDO IMAGEM DOCKER ULTIMA VERSÃO"
    - docker tag ${CONTAINER_IMAGE} cristianofeliciano/crisdesafio6:04
    - docker push ${CONTAINER_IMAGE}

deploy-kubernetes:
  stage: deploy_k8s
  script:
    - apt-get update -y
    - apt-get install docker -y
    - apt-get install curl -y
    - curl -L https://github.com/a8m/envsubst/releases/download/v1.2.0/envsubst-`uname -s`-`uname -m` -o envsubst
    - chmod +x envsubst
    - mv envsubst /usr/local/bin
    - curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.18.0/bin/linux/amd64/kubectl
    - chmod +x ./kubectl
    - mv ./kubectl /usr/local/bin/kubectl
    - mkdir -p ~/.kube/
    - touch ~/.kube/config1 ~/.kube/config
    - echo $KUBE_CONFIG > ~/.kube/config1
    - base64 -d ~/.kube/config1 > ~/.kube/config
    - export "CI_PIPELINE_ID"
    - env | egrep CI_PIPELINE_ID
    - whereis envsubst
    - |
            validacao_ingress=$(kubectl get ingress -n crisdesafio6 | wc -l)
            echo $validacao_ingress
            if  test "$validacao_ingress" = "0"  ; then
            envsubst < deployment-green.yaml | kubectl apply -f - --record
            echo "Instalando Crisdesafio-green"
            kubectl apply -f ingress-green.yaml
            kubectl apply -f services-green.yaml
            sleep 20
            curl -k "http://ab1b65556d029414c816f3f9114c333a-143666297.us-east-1.elb.amazonaws.com/crisdesafio-green"
            sleep 10
            echo "Green se está correto, alterando para Crisdesafio"
            envsubst < deployment.yaml | kubectl apply -f - --record
            echo "Instalando Crisdesafio"
            kubectl apply -f ingress.yaml
            kubectl apply -f services.yaml
            kubectl delete services -n crisdesafio6 green
            sleep 20
            echo "Testando rota /crisdesafio"
            curl -k "http://ab1b65556d029414c816f3f9114c333a-143666297.us-east-1.elb.amazonaws.com/crisdesafio"
            else
            echo "Validando que há um ingress em andamento, testando o /crisdesafio."
            curl -k "http://ab1b65556d029414c816f3f9114c333a-143666297.us-east-1.elb.amazonaws.com/crisdesafio"
            exit 0
            fi
``````

8. **Criar o arquivo deployment.yaml /Desafio6 :**

``````
apiVersion: apps/v1
kind: Deployment
metadata:
  name: crisdesafio6
  namespace: crisdesafio6
  labels: 
    app: crisdesafio6
spec:
  replicas: 3
  selector:
    matchLabels:
      deploy: crisdesafio6
      app: crisdesafio6
  template:
    metadata:
      labels:
        deploy: crisdesafio6
        app: crisdesafio6
    spec:
      containers:
        - name: crisdesafio6
          image: cristianofeliciano/crisdesafio6:04
          imagePullPolicy: Always
          env:
            - name: banco
              valueFrom:
                secretKeyRef:
                  name: desafiocris6
                  key: banco
            - name: user
              valueFrom:
                secretKeyRef:
                  name: desafiocris6
                  key: user
            - name: password
              valueFrom:
                secretKeyRef:
                  name: desafiocris6
                  key: password
            - name: db
              valueFrom:
                secretKeyRef:
                  name: desafiocris6
                  key: db    
            - name: access
              valueFrom:
                secretKeyRef:
                  name: desafiocris6
                  key: access
            - name: secret
              valueFrom:
                secretKeyRef:
                  name: desafiocris6
                  key: secret
            - name: region
              valueFrom:
                secretKeyRef:
                  name: desafiocris6
                  key: region
            - name: fila
              valueFrom:
                secretKeyRef:
                  name: desafiocris6
                  key: fila
            - name: nosql
              valueFrom:
                secretKeyRef:
                  name: desafiocris6
                  key: nosql
            - name: fila
              valueFrom:
                secretKeyRef:
                  name: desafiocris6
                  key: fila
          ports:
            - containerPort: 5000
``````

9. **Criar o arquivo deployment-green.yaml /Desafio6 :**

``````
apiVersion: apps/v1
kind: Deployment
metadata:
  name: crisdesafio6
  namespace: crisdesafio6
  labels: 
    app: crisdesafio6-green
spec:
  replicas: 3
  selector:
    matchLabels:
      deploy: crisdesafio6
      app: crisdesafio6
  template:
    metadata:
      labels:
        deploy: crisdesafio6
        app: crisdesafio6
    spec:
      containers:
        - name: crisdesafio6
          image: cristianofeliciano/crisdesafio6:04
          imagePullPolicy: Always
          env:
            - name: banco
              valueFrom:
                secretKeyRef:
                  name: desafiocris6
                  key: banco
            - name: user
              valueFrom:
                secretKeyRef:
                  name: desafiocris6
                  key: user
            - name: password
              valueFrom:
                secretKeyRef:
                  name: desafiocris6
                  key: password
            - name: db
              valueFrom:
                secretKeyRef:
                  name: desafiocris6
                  key: db    
            - name: access
              valueFrom:
                secretKeyRef:
                  name: desafiocris6
                  key: access
            - name: secret
              valueFrom:
                secretKeyRef:
                  name: desafiocris6
                  key: secret
            - name: region
              valueFrom:
                secretKeyRef:
                  name: desafiocris6
                  key: region
            - name: fila
              valueFrom:
                secretKeyRef:
                  name: desafiocris6
                  key: fila
            - name: nosql
              valueFrom:
                secretKeyRef:
                  name: desafiocris6
                  key: nosql
            - name: fila
              valueFrom:
                secretKeyRef:
                  name: desafiocris6
                  key: fila
          ports:
            - containerPort: 5000
``````
10. **Criar o arquivo ingress.yaml /Desafio6 :**

``````
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: example-ingress
  namespace: crisdesafio6
  annotations:
    ingress.kubernetes.io/rewrite-target: / 
spec:
  rules:
  - http:
      paths:
      - path: /crisdesafio
        backend:
          serviceName: crisdesafio6
          servicePort: 5000
      - path: /crisdesafio-green
        backend:
          serviceName: green
          servicePort: 5000
``````

11. **Criar o arquivo ingress-green.yaml /Desafio6 :**

``````
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: example-ingress
  namespace: crisdesafio6
  annotations:
    ingress.kubernetes.io/rewrite-target: / 
spec:
  rules:
  - http:
      paths:
      - path: /crisdesafio-green
        backend:
          serviceName: green
          servicePort: 5000
      - path: /crisdesafio
        backend:
          serviceName: crisdesafio6
          servicePort: 5000
``````
12. **Criar o arquivo services.yaml /Desafio6 :**
``````
kind: Service
apiVersion: v1
metadata:
    name: crisdesafio6
    namespace: crisdesafio6
spec:
  selector:
    deploy: crisdesafio6
    app: crisdesafio6
  ports:
    - port: 5000
  type: LoadBalancer
``````


13. **Criar o arquivo services-green.yaml /Desafio6 :**
``````
kind: Service
apiVersion: v1
metadata:
    name: green
    namespace: crisdesafio6
spec:
  selector:
    deploy: crisdesafio6
    app: crisdesafio6
  ports:
    - port: 5000
  type: LoadBalancer

``````
14. ** Subir arquivos no Gitlab CI :**

**Comandos:**

- git add.
- git commit -m "versao"
- git push origin master

15. **Executar no GitLab a Pipeline inicial:**

- docker-build-image

16. **Executar no GitLab a Pipeline inicial:**

- docker-build-image
- deploy-kubernetes

*resultado*

``````
service/green created
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    80  100    80    0     0    296 [
  "API: 1.1", 
  "Status 200", 
  "Mysql OK", 
  "Nosql: Ok", 
  "Fila: OK"
]
     0 --:--:-- --:--:-- --:--:--   296
Green se está correto, alterando para Crisdesafio
deployment.apps/crisdesafio6 configured
Instalando Crisdesafio
ingress.extensions/example-ingress configured
service/crisdesafio6 created
service "green" deleted
Testando rota /crisdesafio
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    80  100    80    0     0    689      0 --:--:-- --:--:-- --:--:--   689
[
  "API: 1.0", 
  "Status 200", 
  "Mysql OK", 
  "Nosql: Ok", 
  "Fila: OK"
]
Job succeeded
``````
*Resultado visivel:*

http://ab1b65556d029414c816f3f9114c333a-143666297.us-east-1.elb.amazonaws.com/crisdesafio


**Referencias**

- Kubernetes - https://www.poeticoding.com/create-a-high-availability-kubernetes-cluster-on-aws-with-kops/

- GitLab ─ https://forum.gitlab.com/t/error-during-connect-post-http-docker-2375-v1-40-auth-dial-tcp-lookup-docker-on-169-254-169-254-no-such-host/28678

- GitLab - https://medium.com/devopstricks/ci-cd-with-gitlab-kubernetes-399f81ac91ae

- Kops - https://www.poeticoding.com/create-a-high-availability-kubernetes-cluster-on-aws-with-kops/

- Blue Green - https://www.haproxy.com/blog/rolling-updates-and-blue-green-deployments-with-kubernetes-and-haproxy/

- Tech Primers - https://www.youtube.com/watch?v=YBv-0VzjwhM&start

- Assistanz - https://www.assistanz.com/steps-to-create-custom-namespace-in-the-kubernetes/


















